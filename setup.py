from setuptools import setup

setup(name='data_preparation',
      version='0.1',
      install_requires=['numpy',
                        'torch',
                        'torchvision'])
