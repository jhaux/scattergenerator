import numpy as np
import torch
from torchvision.transforms import Scale, ToTensor
import os
from PIL import Image
from scipy.misc import imresize, imread
from glimpsenet.model.feature_generator import VGG16
from torch.autograd import Variable


class CelebsGenerator(object):

    def __init__(self,
                 imsize=[300, 300],
                 scale_down=True,
                 transforms=None):

        home = os.environ['HOME']
        self.root = os.path.join(home,
                                 'Documents/Uni HD/'
                                 + 'Masterarbeit/datasets/CelebsA/')

        self.images_path = os.path.join(self.root, 'img_celeba')
        suffix = '_all.npy'
        self.labels_path = os.path.join(self.root, '{}'+suffix)
        self.labels = np.load(self.labels_path.format('labels'), 'r')
        self.num_classes = self.labels.shape[-1]
        self.forbidden_indeces = []  # \
        # np.load(self.labels_path.format('forbidden_images'), 'r')
        self.num_im = self.labels.shape[0]
        self.lr_shape = [25, 25]
        self.to_low_res = Scale(self.lr_shape)
        self.imsize = imsize
        self.scale_down = scale_down
        self.l_size = [1, 1]

        self.to_tensor = ToTensor()
        self.transforms = transforms

    def __next__(self):
        return self.next_train()

    def __len__(self):
        return self.num_im

    def __getitem__(self, idx):
        return self.next_train(idx)

    def image_name(self, index):
        return '{:06}.jpg'.format(index)

    def load_image(self, index):
        name = self.image_name(index)
        name = os.path.join(self.images_path, name)
        image = imread(name, mode='RGB')
        if self.scale_down:
            image = imresize(image, self.imsize)
        image = Image.fromarray(image.astype('uint8'),
                                mode='RGB')
        return image

    def translate_label(self, label):
        label = np.array(np.where(label > 0)).T
        # label = np.zeros([self.num_classes])
        # label[labels] = 1
        return label

    def next_train(self, index=None):
        if not index:
            index = np.random.randint(low=1, high=self.num_im)
        while index in self.forbidden_indeces:
            print('Drew forbidden index, redrawing')
            index = np.random.randint(low=1, high=self.num_im)
        image = self.load_image(index)
        label = self.labels[index-1]
        label = self.translate_label(label)

        if self.transforms:
            self.transforms(image)
        low_res = self.to_low_res(image)

        # Convert to Tensor and add batch size of 1
        image = self.to_tensor(image).unsqueeze(0)
        low_res = self.to_tensor(low_res).unsqueeze(0)
        label = torch.from_numpy(label).long().unsqueeze(0)

        return {'features': image, 'low_res': low_res, 'labels': label}


class CelebsGeneratorWithVGG(CelebsGenerator):

    def __init__(self,
                 imsize=[300, 300],
                 scale_down=True,
                 transforms=None,
                 layers=['conv_6.relu']):

        super().__init__(imsize, scale_down, transforms)

        self.vgg = VGG16()
        self.layers = layers

    def next_train(self, index=None):
        sample = super().next_train(index)

        features = Variable(sample['features'])
        sample['image'] = features
        shape = features.size()
        if shape[1] == 1:
            features = features.expand(1, 3, shape[2], shape[3])

        _, features_new = self.vgg(features, self.layers)

        features = torch.zeros(0, 0, 11, 11)
        for n, f in features_new.items():
            features = torch.cat((features, f.data), 1)
        sample['features'] = features

        return sample
