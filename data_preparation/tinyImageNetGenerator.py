import numpy as np
from PIL import Image
import torch
from torchvision.transforms import Scale, ToTensor
import warnings
import os
import sys
# import time
from tqdm import trange, tqdm
from glimpsenet.model.feature_generator import VGG16
from glimpsenet.util.tensor_ops import to_array, to_variable, to_tensor
from torch.autograd import Variable

import pickle


VAL_ROOT = '/media/johannes/Data 1/Masterarbeit/Datasets/validation_sets'
DSET_NAME = 'tiny_imagenet_{}'
FEATURE_NAME = '_features_{}_{}'


class TinyImagenetGenerator(object):
    '''Data Generation object, that produces new image samples on the fly
    to ensure, no sample is seen twice. This should make overfitting more
    unlikely.'''

    def __init__(self,
                 subset='my_wnids.txt',
                 transforms=None):
        '''
        Arguments:
            transforms: Callable, e.g. Pytorch Transforms to be applied to the
                images
        '''

        self.lr_shape = [16, 16]
        self.to_low_res = Scale(self.lr_shape)
        self.to_tensor = ToTensor()
        self.transforms = transforms

        self.root = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/tiny-imagenet-200')

        self.classes = np.genfromtxt(os.path.join(self.root, subset),
                                     dtype=str)
        self.num_classes = len(self.classes)
        self.boxes = {}
        for c in self.classes:
            box_root = os.path.join(self.root,
                                    'train',
                                    c,
                                    '{}_boxes.txt'.format(c))
            b = np.genfromtxt(box_root, dtype=str)
            self.boxes[c] = {}
            for i in range(500):
                x1, y1, x2, y2 = b[i][1:]
                self.boxes[c][i] = [int(x1), int(y1),
                                    int(x2)-int(x1), int(y2)-int(y2)]

        self.num_samples = len(self.classes) * 500

    def __next__(self):
        return self.next_train()

    def __len__(self):
        warnings.warn('This Dataset is of infinite size. len() just returns '
                      'just a pretty large number.', Warning)
        return sys.maxsize

    def __getitem__(self, idx):
        np.random.seed(self.start + idx)
        self.initial = False
        return self.next_train()

    def next_train(self):
        accept = False
        while not accept:
            c = np.random.randint(self.num_classes)
            i = np.random.randint(500)

            impath = os.path.join(self.root,
                                  'train',
                                  self.classes[c],
                                  'images')
            image = os.listdir(impath)[i]
            impath = os.path.join(impath, image)
            image = Image.open(impath)
            if image.mode == 'RGB':
                accept = True

        if self.transforms:
            self.transforms(image)
        low_res = self.to_low_res(image)

        # Convert to Tensor and add batch size of 1
        image = self.to_tensor(image).unsqueeze(0)
        low_res = self.to_tensor(low_res).unsqueeze(0)
        label = torch.from_numpy(np.array([c])).long().unsqueeze(0)

        location = self.boxes[self.classes[c]][i][:2]
        w = self.boxes[self.classes[c]][i][2]
        h = self.boxes[self.classes[c]][i][3]

        return {'features': image, 'low_res': low_res, 'labels': label,
                'locations': [location], 'indeces': [i], 'bounds': [w, h]}


class TinyImagenetGeneratorWithFeatures(TinyImagenetGenerator):

    def __init__(self,
                 subset='my_wnids.txt',
                 layers=['conv_6.relu'],
                 which='vgg'):
        '''
        Arguments:
            dataset: numpy array containing the base data (e.g MNIST numbers)
            numObjs: Number of objects to appear in the image
            difficulty: integer value, that detemines how the numbers are
                placed in the image
            imsize: size of the returned image. If values are 2 element lists
                a random size will be drawn within the bounds of these values.
            mode: Distinguish between train and test -> Get rid of that!
            transforms: Callable, e.g. Pytorch Transforms to be applied to the
                images
        '''

        super().__init__(subset)

        self.which = which
        if self.which == 'vgg':
            self.featureG = VGG16()
        elif self.which == 'lenet':
            file_path = os.path.realpath(__file__).split('/')
            parent = '/'.join(file_path[:-1])

            path_of_model = os.path.join(
                    parent,
                    "lenet.tar"
                    )
            self.featureG = torch.load(path_of_model, pickle_module=pickle)
        else:
            raise ValueError('No feature generating function specified')
        self.layers = layers

    def next_train(self):
        sample = super().next_train()

        features = Variable(sample['features'])
        sample['image'] = features
        shape = features.size()
        if shape[1] == 1 and self.which == 'vgg':
            features = features.expand(1, 3, shape[2], shape[3])

        if self.which == 'vgg':
            _, features_new = self.featureG(features, self.layers)
        elif self.which == 'lenet':
            conv1, conv2 = self.featureG(features, True)
            if 'conv1' in self.layers:
                features_new = {'conv1': conv1}
            else:
                features_new = {'conv2': conv2}

        features = torch.zeros(0, 0, 11, 11)
        for n, f in features_new.items():
            features = torch.cat((features, f.data), 1)
        sample['features'] = features

        return sample


def get_validation_set(dset, n_o, dif, imsize, layers, which):
    val_root = VAL_ROOT

    dx, dy = imsize
    dset_name = DSET_NAME.format(dset, n_o, dif, dx, dy)

    dset_base_path = os.path.join(val_root, dset_name)
    images_path = dset_base_path + '_images.npy'
    labels_path = dset_base_path + '_labels.npy'
    locs_path = dset_base_path + '_locs.npy'
    index_path = dset_base_path + '_idxs.npy'
    lowres_path = dset_base_path + '_lows.npy'
    if os.path.isfile(images_path):
        images = np.load(images_path, 'r')
        labels = np.load(labels_path, 'r')
        locs = np.load(locs_path, 'r')
        index = np.load(index_path, 'r')
        lowres = np.load(lowres_path, 'r')
    else:
        images, labels, locs, index, lowres = \
                save_validation_set(dset, n_o, dif, imsize)

    features = None
    if layers and which:
        features_name = dset_name + FEATURE_NAME.format(layers, which)
        feature_path = os.path.join(dset_base_path, features_name)
        if os.path.isfile(feature_path):
            features = np.load(feature_path, 'r')
        else:
            features = save_features(dset_base_path, layers, which)

    return images, labels, locs, index, lowres, features


def save_validation_set(subset, dset_size=2500):
    gen = TinyImagenetGenerator(subset)

    print('Saving validation set...')

    images = []
    labels = []
    locs = []
    idxs = []
    lows = []
    for i in trange(dset_size):
        res = next(gen)
        images.append(res['features'])
        labels.append(res['labels'])
        locs.append(res['locations'])
        idxs.append(res['indeces'])
        lows.append(res['low_res'])

    images = np.stack(to_array(images))
    labels = np.stack(to_array(labels))
    locs = np.array(locs)
    idxs = np.array(idxs)
    lows = np.stack(to_array(lows))

    dset_name = DSET_NAME.format(subset)
    dset_base_path = os.path.join(VAL_ROOT, dset_name)

    np.save(dset_base_path + '_images', images)
    np.save(dset_base_path + '_labels', labels)
    np.save(dset_base_path + '_locs', locs)
    np.save(dset_base_path + '_idxs', idxs)
    np.save(dset_base_path + '_lows', lows)

    return images, labels, locs, idxs, lows


def save_features(dset_base_path, layers, which):
    if which == 'vgg':
        featureG = VGG16()
    elif which == 'lenet':
        file_path = os.path.realpath(__file__).split('/')
        parent = '/'.join(file_path[:-1])

        path_of_model = os.path.join(
                parent,
                "lenet.tar"
                )
        featureG = torch.load(path_of_model, pickle_module=pickle)

    features_path = dset_base_path + FEATURE_NAME.format(layers, which)
    images = np.load(dset_base_path + '_images.npy')
    i = 0
    for batch in tqdm(np.array_split(images, 25)):
        features = []
        if not os.path.isfile(features_path + '_{}.npy'.format(i)):
            for image in tqdm(batch):
                image = to_variable(image)
                shape = image.size()
                if shape[1] == 1 and which == 'vgg':
                    image = image.expand(1, 3, shape[2], shape[3])

                if which == 'vgg':
                    _, features_new = featureG(image, layers)
                elif which == 'lenet':
                    conv1, conv2 = featureG(features, True)
                    if 'conv1' in layers:
                        features_new = {'conv1': conv1}
                    else:
                        features_new = {'conv2': conv2}

                F = torch.zeros(0, 0, 11, 11)
                for n, f in features_new.items():
                    F = torch.cat((F, f.data), 1)

                features.append(to_array(F))

            features = np.stack(features)
            np.save(features_path + '_{}'.format(i), features)
        i += 1

    features = []
    for i in range(25):
        features.append(np.load(features_path + '_{}.npy'.format(i), 'r'))

    features = np.concatenate(features)
    np.save(features_path, features)

    return features


class GMValidationGenerator(object):

    def __init__(self,
                 dset='mnist',
                 n_o=2,
                 dif=1,
                 imsize=[100, 100],
                 layers=None,
                 which=None):
        data = get_validation_set(dset, n_o, dif, imsize, layers, which)
        self.data = list(data)
        if self.data[5] is None:
            self.data[5] = len(self.data[0]) * [None]

        if dset == 'mnist':
            self.num_classes = 10
        else:
            raise ValueError('Currently only mnist supported')

        self.index = 0

    def __len__(self):
        return len(self.data[0])

    def __next__(self):
        I, L, loc, idx, low, f = list(zip(*list(self.data)))[self.index]
        I = to_tensor(I)
        L = to_tensor(L).squeeze(0).type('torch.LongTensor')
        low = to_tensor(low)
        if f is not None:
            features = to_tensor(f)
        else:
            features = I

        self.index += 1

        return {'features': features, 'low_res': low, 'labels': L,
                'locations': loc, 'indeces': idx,
                'image': I}

    def reset_valid_counter(self):
        self.index = 0


if __name__ == '__main__':
    gen = GMValidationGenerator(which='vgg', layers=['conv_6.relu'])
    for i in gen:
        print(i)
        break
