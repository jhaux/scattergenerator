import os
import numpy as np
from tqdm import tqdm


DSET_ROOT = '/home/johannes/Documents/Uni HD/Masterarbeit/datasets/fashion'


def load_mnist(kind='train'):
    import os
    import gzip
    import numpy as np

    """Load MNIST data from `path`"""

    path = os.path.join('/home/johannes/Documents/',
                        'Uni HD/Masterarbeit/datasets/fashion/',
                        'fashion-mnist/data/fashion')

    labels_path = os.path.join(path,
                               '%s-labels-idx1-ubyte.gz'
                               % kind)
    images_path = os.path.join(path,
                               '%s-images-idx3-ubyte.gz'
                               % kind)

    with gzip.open(labels_path, 'rb') as lbpath:
        labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                               offset=8)

    with gzip.open(images_path, 'rb') as imgpath:
        images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                               offset=16).reshape(len(labels), 784)

    return images, labels


def store_numpy(root=DSET_ROOT):
    for mode in tqdm(['train', 'eval']):
        kind = mode if mode == 'train' else 't10k'
        ims, ls = load_mnist(kind=kind)
        ims = np.reshape(ims, [-1, 28, 28, 1])
        ims = ims / 255.

        for n, dset in zip(['im', 'l'], [ims, ls]):
            savepath = os.path.join(root, 'fashion_{}_{}.npy'.format(mode, n))
            np.save(savepath, dset)
            print(dset.shape)


if __name__ == '__main__':
    store_numpy()
