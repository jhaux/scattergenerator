import numpy as np
from PIL import Image
import torch
from torchvision.transforms import Scale, ToTensor
import warnings
import os
import sys
import time
from tqdm import trange, tqdm
from glimpsenet.model.feature_generator import VGG16
from glimpsenet.util.tensor_ops import to_array, to_variable, to_tensor
from torch.autograd import Variable

import pickle


VAL_ROOT = '/media/johannes/Data 1/Masterarbeit/Datasets/validation_sets'
DSET_NAME = '{}_n{}_d{}_s{}x{}'
FEATURE_NAME = '_features_{}_{}'


class ScatterGenerator(object):
    '''Data Generation object, that produces new image samples on the fly
    to ensure, no sample is seen twice. This should make overfitting more
    unlikely.'''

    def __init__(self,
                 dataset,
                 num_objs,
                 start_difficulty,
                 max_difficulty=6,
                 imsize=[100, 100],
                 lr_shape=[25, 25],
                 mode='train',
                 transforms=None):
        '''
        Arguments:
            dataset: numpy array containing the base data (e.g MNIST numbers)
            numObjs: Number of objects to appear in the image
            difficulty: integer value, that detemines how the numbers are
                placed in the image
            imsize: size of the returned image. If values are 2 element lists
                a random size will be drawn within the bounds of these values.
            mode: Distinguish between train and test -> Get rid of that!
            transforms: Callable, e.g. Pytorch Transforms to be applied to the
                images
        '''

        self.num_objs = num_objs
        self.start_difficulty = start_difficulty
        self.max_difficulty = max_difficulty
        self.imsize = imsize
        self.sort_by = 'y'
        self.mode = mode
        self.num_classes = 10
        self.a_0 = 125
        self.m_0 = 0.5
        self.lr_shape = lr_shape
        self.to_low_res = Scale(self.lr_shape)
        self.to_tensor = ToTensor()
        self.transforms = transforms

        self.dset_root = dset_root = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/')
        if dataset == 'mnist':
            self.dset_im = dset_im = 'MNIST/mnist_complete_im.npy'
            self.dset_l = dset_l = 'MNIST/mnist_complete_l.npy'
        elif dataset == 'fashion':
            if mode == 'train':
                self.dset_im = dset_im = 'fashion/fashion_train_im.npy'
                self.dset_l = dset_l = 'fashion/fashion_train_l.npy'
            else:
                self.dset_im = dset_im = 'fashion/fashion_eval_im.npy'
                self.dset_l = dset_l = 'fashion/fashion_eval_l.npy'

        self.images = np.load(os.path.join(dset_root, dset_im), 'r')
        self.labels = np.load(os.path.join(dset_root, dset_l), 'r')

        self.num_samples = len(self.images)
        self.patch_size = self.images[0].shape[0]

        if self.patch_size != self.images[0].shape[1]:
            raise ValueError('Images supplied to this generator need to be'
                             'square for now, but are {}x{}'
                             .format(*self.images[0].shape[0:2]))

        self.start = int(str(time.time())[-10:].replace('.', ''))
        self.initial = True  # Used for seeding in __getitem__

    def __next__(self):
        return self.next_train()

    def __len__(self):
        warnings.warn('This Dataset is of infinite size. len() just returns '
                      'just a pretty large number.', Warning)
        return sys.maxsize

    def __getitem__(self, idx):
        np.random.seed(self.start + idx)
        self.initial = False
        return self.next_train()

    def next_train(self):
        difficulty = self.start_difficulty  # for now
        if np.ndim(self.imsize) == 2:
            [xl, xh], [yl, yh] = self.imsize
            dx = np.random.randint(low=xl, high=xh)
            dy = np.random.randint(low=yl, high=yh)
            imsize = [dx, dy]
        else:
            imsize = self.imsize
        image = np.zeros(imsize + [1])
        label = np.zeros([self.num_objs, 1])
        # image, label index -> tuple in object dimension [n_objs, im_d, l_d]
        idx_tuple = np.random.choice(self.num_samples, [self.num_objs])

        location_tuple = self.generateDistinctLocations(self.num_objs,
                                                        difficulty,
                                                        imsize)

        ps = self.patch_size
        for (x, y), idx in zip(location_tuple, idx_tuple):
            image[x:x+ps, y:y+ps] = self.images[idx]

        idx_tuple, perm = self.sortIndexTuple(location_tuple, idx_tuple)
        sorted_location_tuple = location_tuple[perm]
        label = np.vstack([self.labels[j] for j in idx_tuple])

        image = Image.fromarray((255*image[..., 0]).astype('uint8'), mode='L')
        if self.transforms:
            self.transforms(image)
        low_res = self.to_low_res(image)

        # Convert to Tensor and add batch size of 1
        image = self.to_tensor(image).unsqueeze(0)
        low_res = self.to_tensor(low_res).unsqueeze(0)
        label = torch.from_numpy(label).long().unsqueeze(0)

        return {'features': image, 'low_res': low_res, 'labels': label,
                'locations': sorted_location_tuple, 'indeces': idx_tuple}

    def generateDistinctLocations(self, numObjs, difficulty, imsize,
                                  possible_area=1.0):
        '''Generates a location tuple (l_1, l_2) with l_i = (x_i, y_i)
        such that the images put at those locations do not overlap'''
        imsize = np.array(imsize)
        if type(possible_area) == float:
            possible_area = possible_area * imsize
        else:
            possible_area = np.array(possible_area)
        delta = (imsize - np.array(possible_area)) / 2.

        # first round: random tuple
        max_x, max_y = np.array(possible_area) - self.patch_size + delta
        min_x, min_y = delta

        if numObjs == 1:
            # Location drawn from beta-distribution, that broadens with
            # difficulty
            if difficulty == 0:
                # Place exactly at center
                x1 = (max_x + min_x) / 2
                y1 = (max_y + min_y) / 2
            else:
                # Place Beta distributed
                a = self.alpha(difficulty)
                x1, y1 = np.random.beta(a=a, b=a, size=[2])
                # Scale to desired range
                x1 = x1 * (max_x - min_x) + min_x
                y1 = y1 * (max_y - min_y) + min_y

            locations = [[x1, y1]]
            return np.array(locations, dtype=int)

        elif numObjs > 1:
            # Initial location drawn from uniform distribution
            x1 = np.random.randint(min_x, max_x)
            y1 = np.random.randint(min_y, max_y)

            locations = [[x1, y1]]

        for i in range(numObjs - 1):
            redraw = True
            while redraw:
                m = self.steepnessFromDifficulty(difficulty)

                def line(x, x1, y1, m):
                    offset = y1 - x1*m
                    return m*x + offset

                def draw_y(y1, max_y=max_y):
                    n_iter = 0
                    valid = True
                    y2 = np.random.randint(min_y, max_y)
                    while y2 <= y1+self.patch_size and y2 > y1-self.patch_size:
                        if n_iter >= 10:
                            print('Had to redraw')
                            valid = False
                            break
                        y2 = np.random.randint(min_x, max_y)
                        n_iter += 1
                    return y2, valid

                accept = False
                while not accept:
                    y2, valid = draw_y(y1)
                    if not valid:
                        break
                    x_stop1 = line(y2, y1, x1, -m)
                    x_stop2 = line(y2, y1, x1, m)
                    x_min, x_max = np.sort([x_stop1, x_stop2])

                    x_min = np.ceil(x_min)
                    x_min = np.max([0, x_min])
                    x_max = np.floor(x_max)
                    x_max = np.min([max_x, x_max])

                    if x_min == x_max:
                        continue
                    x2 = np.random.randint(x_min, x_max)
                    accept = True
                    redraw = False

            locations.append([x2, y2])

            if np.sqrt((x1-x2)**2 + (y1-y2)**2) < 28:
                # Debug output - This should never occur
                print(x1, y1)
                print(x2, y2)
                print('\n')

        return np.array(locations, dtype=int)

    def sortIndexTuple(self, location_tuple, index_tuple):
        if self.sort_by is None:
            return index_tuple

        idx = 0 if self.sort_by == 'x' else 1
        l_slice = location_tuple[:, idx]

        indeces = l_slice.argsort()
        index_tuple = np.array(index_tuple)[indeces]

        return index_tuple, indeces

    def steepnessFromDifficulty(self, difficulty):
        return self.m_0 * 10 ** (2 * difficulty)

    def alpha(self, difficulty):
        d_range = self.max_difficulty
        d = difficulty
        a = self.a_0 * ((1 + float(d_range - d)) / float(d_range))**d

        return a


class ScatterGeneratorWithFeatures(ScatterGenerator):

    def __init__(self,
                 dataset,
                 num_objs,
                 start_difficulty,
                 max_difficulty=6,
                 imsize=[100, 100],
                 lr_shape=[25, 25],
                 mode='train',
                 transforms=None,
                 layers=['conv_6.relu'],
                 which='vgg'):
        '''
        Arguments:
            dataset: numpy array containing the base data (e.g MNIST numbers)
            numObjs: Number of objects to appear in the image
            difficulty: integer value, that detemines how the numbers are
                placed in the image
            imsize: size of the returned image. If values are 2 element lists
                a random size will be drawn within the bounds of these values.
            mode: Distinguish between train and test -> Get rid of that!
            transforms: Callable, e.g. Pytorch Transforms to be applied to the
                images
        '''

        super().__init__(dataset,
                         num_objs,
                         start_difficulty,
                         max_difficulty=max_difficulty,
                         imsize=imsize,
                         lr_shape=lr_shape,
                         mode=mode,
                         transforms=transforms)

        self.which = which
        if self.which == 'vgg':
            self.featureG = VGG16()
        elif self.which == 'lenet':
            file_path = os.path.realpath(__file__).split('/')
            parent = '/'.join(file_path[:-1])

            path_of_model = os.path.join(
                    parent,
                    "lenet.tar"
                    )
            self.featureG = torch.load(path_of_model, pickle_module=pickle)
        else:
            raise ValueError('No feature generating function specified')
        self.layers = layers

    def next_train(self):
        sample = super().next_train()

        features = Variable(sample['features'])
        sample['image'] = features
        shape = features.size()
        if shape[1] == 1 and self.which == 'vgg':
            features = features.expand(1, 3, shape[2], shape[3])

        if self.which == 'vgg':
            _, features_new = self.featureG(features, self.layers)
        elif self.which == 'lenet':
            conv1, conv2 = self.featureG(features, True)
            if 'conv1' in self.layers:
                features_new = {'conv1': conv1}
            else:
                features_new = {'conv2': conv2}

        features = torch.zeros(0, 0, 11, 11)
        for n, f in features_new.items():
            features = torch.cat((features, f.data), 1)
        sample['features'] = features

        return sample


def get_validation_set(dset, n_o, dif, imsize, lr_shape, layers, which):
    val_root = VAL_ROOT

    dx, dy = imsize
    lx, ly = lr_shape
    dset_name = DSET_NAME.format(dset, n_o, dif, dx, dy)
    if lr_shape != [25, 25]:
        dset_name += '_lr{}-{}'.format(lx, ly)

    dset_base_path = os.path.join(val_root, dset_name)
    images_path = dset_base_path + '_images.npy'
    labels_path = dset_base_path + '_labels.npy'
    locs_path = dset_base_path + '_locs.npy'
    index_path = dset_base_path + '_idxs.npy'
    lowres_path = dset_base_path + '_lows.npy'
    if os.path.isfile(images_path):
        images = np.load(images_path, 'r')
        labels = np.load(labels_path, 'r')
        locs = np.load(locs_path, 'r')
        index = np.load(index_path, 'r')
        lowres = np.load(lowres_path, 'r')
    else:
        images, labels, locs, index, lowres = \
                save_validation_set(dset, n_o, dif, imsize, lr_shape)

    features = None
    if layers and which:
        features_name = dset_name + FEATURE_NAME.format(layers, which)
        feature_path = os.path.join(dset_base_path, features_name)
        if os.path.isfile(feature_path):
            features = np.load(feature_path, 'r')
        else:
            features = save_features(dset_base_path, layers, which)

    return images, labels, locs, index, lowres, features


def save_validation_set(dset, n_o, dif, imsize, lr_shape, dset_size=2500):
    gen = ScatterGenerator(dset, n_o, dif, imsize=imsize, lr_shape=lr_shape,
                           mode='eval')

    print('Saving validation set...')
    print(dset, n_o, dif, imsize)

    images = []
    labels = []
    locs = []
    idxs = []
    lows = []
    for i in trange(dset_size):
        res = next(gen)
        images.append(res['features'])
        labels.append(res['labels'])
        locs.append(res['locations'])
        idxs.append(res['indeces'])
        lows.append(res['low_res'])

    images = np.stack(to_array(images))
    labels = np.stack(to_array(labels))
    locs = np.array(locs)
    idxs = np.array(idxs)
    lows = np.stack(to_array(lows))

    dx, dy = imsize
    lx, ly = lr_shape
    dset_name = DSET_NAME.format(dset, n_o, dif, dx, dy)
    if lr_shape != [25, 25]:
        dset_name += '_lr{}-{}'.format(lx, ly)
    dset_base_path = os.path.join(VAL_ROOT, dset_name)

    np.save(dset_base_path + '_images', images)
    np.save(dset_base_path + '_labels', labels)
    np.save(dset_base_path + '_locs', locs)
    np.save(dset_base_path + '_idxs', idxs)
    np.save(dset_base_path + '_lows', lows)

    return images, labels, locs, idxs, lows


def save_features(dset_base_path, layers, which):
    if which == 'vgg':
        featureG = VGG16()
    elif which == 'lenet':
        file_path = os.path.realpath(__file__).split('/')
        parent = '/'.join(file_path[:-1])

        path_of_model = os.path.join(
                parent,
                "lenet.tar"
                )
        featureG = torch.load(path_of_model, pickle_module=pickle)

    features_path = dset_base_path + FEATURE_NAME.format(layers, which)
    images = np.load(dset_base_path + '_images.npy')
    i = 0
    for batch in tqdm(np.array_split(images, 25)):
        features = []
        if not os.path.isfile(features_path + '_{}.npy'.format(i)):
            for image in tqdm(batch):
                image = to_variable(image)
                shape = image.size()
                if shape[1] == 1 and which == 'vgg':
                    image = image.expand(1, 3, shape[2], shape[3])

                if which == 'vgg':
                    _, features_new = featureG(image, layers)
                elif which == 'lenet':
                    conv1, conv2 = featureG(features, True)
                    if 'conv1' in layers:
                        features_new = {'conv1': conv1}
                    else:
                        features_new = {'conv2': conv2}

                F = torch.zeros(0, 0, 11, 11)
                for n, f in features_new.items():
                    F = torch.cat((F, f.data), 1)

                features.append(to_array(F))

            features = np.stack(features)
            np.save(features_path + '_{}'.format(i), features)
        i += 1

    features = []
    for i in range(25):
        features.append(np.load(features_path + '_{}.npy'.format(i), 'r'))

    features = np.concatenate(features)
    np.save(features_path, features)

    return features


class GMValidationGenerator(object):

    def __init__(self,
                 dset='mnist',
                 n_o=2,
                 dif=1,
                 imsize=[100, 100],
                 lr_shape=[25, 25],
                 layers=None,
                 which=None):
        data = get_validation_set(dset, n_o, dif, imsize, lr_shape,
                                  layers, which)
        self.data = list(data)
        if self.data[5] is None:
            self.data[5] = len(self.data[0]) * [None]

        if dset == 'mnist' or dset == 'fashion':
            self.num_classes = 10
        else:
            raise ValueError('Currently only mnist supported')

        self.index = 0

    def __len__(self):
        return len(self.data[0])

    def __next__(self):
        I, L, loc, idx, low, f = list(zip(*list(self.data)))[self.index]
        I = to_tensor(I)
        L = to_tensor(L).squeeze(0).type('torch.LongTensor')
        low = to_tensor(low)
        if f is not None:
            features = to_tensor(f)
        else:
            features = I

        self.index += 1

        return {'features': features, 'low_res': low, 'labels': L,
                'locations': loc, 'indeces': idx,
                'image': I}

    def reset_valid_counter(self):
        self.index = 0


if __name__ == '__main__':
    gen = GMValidationGenerator(which='vgg', layers=['conv_6.relu'])
    for i in gen:
        print(i)
        break
