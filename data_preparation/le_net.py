__author__ = 'Hanzhou Wu'

import torch
import os
from tqdm import trange, tqdm

from torch import nn, optim
from torch.autograd import Variable
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor

import pickle
print('='*20)
print(pickle)
print('='*20)


class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, (5, 5), padding=2)
        self.conv2 = nn.Conv2d(6, 16, (5, 5))
        self.fc1 = nn.Linear(16*5*5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x, get_features=False):
        x1 = F.relu(self.conv1(x))
        x2 = F.max_pool2d(x1, (2, 2))
        x2 = F.relu(self.conv2(x2))
        if not get_features:
            x = F.max_pool2d(x2, (2, 2))
            x = x.view(-1, self.num_flat_features(x))
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x
        else:
            return x1, x2

    def num_flat_features(self, x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


if __name__ == '__main__':
    # net = LeNet()
    # file_path = os.path.realpath(__file__).split('/')
    # parent = '/'.join(file_path[:-1])

    # path_of_weights = os.path.join(
    #         parent,
    #         "lenet_weights.torch.tar"
    #         )
    # print(path_of_weights)
    # if os.path.exists(parent):
    #     net.load_state_dict(torch.load(path_of_weights))
    # else:
    #     raise ValueError("LeNet is uninitialized")

    net = torch.load('lenet.tar', pickle_module=pickle)

    print(net)

    use_gpu = torch.cuda.is_available()
    if use_gpu:
        net = net.cuda()
        print('USE GPU')
    else:
        print('USE CPU')

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001)

    print ("1. Loading data")
    bs = 32
    mnist = MNIST('.', train=True, download=True, transform=ToTensor())
    trainLoader = DataLoader(mnist, batch_size=bs, shuffle=True)
    mnist_test = MNIST('.', train=False, download=True, transform=ToTensor())
    testLoader = DataLoader(mnist_test, batch_size=1, shuffle=False)

    print ("2. Training phase")
    nb_train = len(trainLoader)
    nb_epoch = 10
    nb_index = 0

    if os.path.isfile("./lenet_weights.torch"):
        net.load_state_dict(torch.load("./lenet_weights.torch"))

    if False:
        for epoch in trange(nb_epoch):
            for i, (mini_data, mini_label) in tqdm(enumerate(trainLoader, 0),
                                                   total=len(trainLoader)):
                if nb_index + bs >= nb_train:
                    nb_index = 0
                else:
                    nb_index = nb_index + bs

            mini_data = Variable(mini_data)
            mini_label = Variable(mini_label)

            if use_gpu:
                mini_data = mini_data.cuda()
                mini_label = mini_label.cuda()
            mini_pred = net(mini_data)

            optimizer.zero_grad()
            mini_loss = criterion(mini_pred, mini_label)
            mini_loss.backward()
            optimizer.step()

            if (i + 1) % 2000 == 0:
                print("Epoch = %d, Loss = %f" % (epoch+1, mini_loss.data[0]))
                if epoch < 2001:
                    print("Saving weights")
                torch.save(net.state_dict(), "lenet_weights.torch.tar",
                           pickle_module=pickle)
    print('Saving net on cpu?')
    torch.save(net.state_dict(), "lenet_weights.torch.tar",
               pickle_module=pickle)
    torch.save(net.cpu(), "lenet.tar",
               pickle_module=pickle)

    print ("3. Testing phase")
    net.eval()
    net = net.cuda()

    accuracy = 0.
    for i, (X, Y) in tqdm(enumerate(testLoader), total=len(testLoader)):
        sample_data = Variable(X)
        sample_label = Y

        if use_gpu:
            sample_data = sample_data.cuda()
            sample_label = sample_label.cuda()
        sample_out = net(sample_data)
        _, pred = torch.max(sample_out, 1)

        eq = torch.eq(sample_label, pred.data)
        accuracy += float(eq.float().mean())
    accuracy /= len(testLoader)

    print(accuracy)
